# run3_ghost_prob
## In cern
## Install stack 
```
curl https://gitlab.cern.ch/rmatev/lb-stack-setup/raw/master/setup.py | python3 - tuples
cd tuples
lhcb-proxy-init
kinit -r 7d user@CERN.CH
make BINARY_TAG=x86_64_v2-centos7-gcc12+detdesc-opt Moore
``` 
Copy new `UpgradeGhostId.cpp` to  `Rec/Tr/TrackTools/src/UpgradeGhostId.cpp`, `UpgradeGhostIdNT.cpp` to `Rec/Tr/TrackMCTools/src/UpgradeGhostIdNT.cpp` and `TrackAddNNGhostId.cpp` to `Rec/Tr/TrackUtils/src/TrackAddNNGhostId.cpp 
Add one line `src/TrackAddNNGhostId.cpp` at `Rec/Tr/TrackUtils/CMakeLists.txt'

then  make again

``` 
make BINARY_TAG=x86_64_v2-centos7-gcc12+detdesc-opt Moore
```

# Tuple
Copy new `mc_checking.py` to  `Moore/Hlt/RecoConf/python/RecoConf/mc_checking.py`. Copy `skim.py`, `skim.qmt` and `input_and_conds.py` to master catalogue. The first generation suggested changing the `options.evt_max = -1` in `input_and_conds.py` to 100. 


```
./Moore/build.x86_64_v2-centos7-gcc12+detdesc-opt/run gaudirun.py skim.qmt
```
## MVA trainning
Achieved by Sigmoid directory

