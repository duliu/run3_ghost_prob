1. Complile:


  - In CCNU computer farm (hepfarm04, 05):
     - source setup_local.sh
     - make

  - In lxplus:
     - source setup_cvmfs.sh
     - make 


2. Ghost Prob study:


Take long track as example, `-t 3`

  - TMVA training:
     - ./run_ghostP -c inputs/parameters.rc_TMVA -t 3
     
  - TMVA evaluation:
     - ./run_eval -c inputs/parameters.rc_Eval -t 3  

  - TMVA plotting:
     - ./run_ghostP -c inputs/parameters.rc_Plot -t 3

  - TMVA flatten:
     - ./run_ghostP -c inputs/parameters.rc_Flat -t 3


3. More scripts:
   
   For evaluation, plotting and flatten, you could try to use scripts as:

   - script_eval.sh  
   - script_flat.sh  
   - script_plot.sh

