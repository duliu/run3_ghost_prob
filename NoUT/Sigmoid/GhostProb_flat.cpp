#define GhostProb_flat_cxx
// The class definition in GhostProb_flat.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.

// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// root> T->Process("GhostProb_flat.C")
// root> T->Process("GhostProb_flat.C","some options")
// root> T->Process("GhostProb_flat.C+")
//

#include "GhostProb_flat.h"
#include <TH2.h>
#include <TStyle.h>
#include <fstream>

void GhostProb_flat::Begin(TTree * /*tree*/) {
  // The Begin() function is called at the start of the query.
  // When running with PROOF Begin() is only called on the client.
  // The tree argument is deprecated (on PROOF 0 is passed).

  TString option = GetOption();
  system("mkdir -p outputs/");

  ReadParameters();
  Init_hist();

  n_events = 0;
}

void GhostProb_flat::SlaveBegin(TTree * /*tree*/) {
  // The SlaveBegin() function is called after the Begin() function.
  // When running with PROOF SlaveBegin() is called on each slave server.
  // The tree argument is deprecated (on PROOF 0 is passed).
  TString option = GetOption();
}

Bool_t GhostProb_flat::Process(Long64_t entry) {
  // The Process() function is called for each entry in the tree (or possibly
  // keyed object in the case of PROOF) to be processed. The entry argument
  // specifies which entry in the currently loaded tree is to be processed.
  // It can be passed to either GhostProb_flat::GetEntry() or TBranch::GetEntry()
  // to read either all or the required parts of the data. When processing
  // keyed objects with PROOF, the object is already loaded and is available
  // via the fObject pointer.
  //
  // This function should contain the "body" of the analysis. It can contain
  // simple or elaborate selection criteria, run algorithms on the data
  // of the event and typically fill histograms.
  //
  // The processing can be stopped by calling Abort().
  //
  // Use fStatus to set the return value of TTree::Process().
  //
  // The return value is currently not used.

  fChain->GetTree()->GetEntry(entry);
  n_events++;
  if (n_events % 1000000 == 0)
    printf("Processing %2d.0 M events!\n", (n_events / 1000000));

  if (classID == 0) {
    // cout<< classID<<"  "<<className<<"  "<<MLP<<"  "<<prob_MLP<<endl;
    float retval = const_num::transform(MLP);
    _ghostprob_hists->getTH1DptrByName("hist_MLP_sig")->Fill(MLP);
    _ghostprob_hists->getTH1DptrByName("hist_MLP_sig_invert")->Fill(retval);
  }
  // flat background MLP
  if (classID == 1 && _flatBKG) {
    float retval = const_num::transform(MLP);
    MLP_vector.push_back(retval);
    _ghostprob_hists->getTH1DptrByName("hist_MLP_bkg")->Fill(MLP);
    _ghostprob_hists->getTH1DptrByName("hist_MLP_bkg_invert")->Fill(retval);
    // cout<<className<<"  "<<MLP<<"  "<<prob_MLP<<endl;
  }

  return kTRUE;
}

void GhostProb_flat::SlaveTerminate() {
  // The SlaveTerminate() function is called after all entries or objects
  // have been processed. When running with PROOF SlaveTerminate() is called
  // on each slave server.

  cout << "Processed "
       << n_events << " track candidates" << endl;

  std::sort(MLP_vector.begin(), MLP_vector.end());

  // Calculate output stuff
  double output[_nobin + 1];
  double input[_nobin + 1];
  if (_invert) {
    output[0] = 1.f;
    for (int i = 1; i < _nobin; i++) {
      output[i] = 1.f - (float)i / _nobin;
    }
    output[_nobin] = 0.f;
  } else {
    output[0] = 0.f;
    for (int i = 1; i < _nobin; i++) {
      output[i] = (float)i / _nobin;
    }
    output[_nobin] = 1.f;
  }

  input[0] = 0.f;
  for (int i = 1; i < _nobin; i++) {
    input[i] = MLP_vector[(MLP_vector.size() * i) / _nobin];
  }
  input[_nobin] = 1.f;

  // print out information
  // Save as pseudo script
  std::ofstream FileStream;
  FileStream.open(_outputtext.c_str());

  FileStream << "// Need to add use RichDet v* Det to requirements file" << std::endl;
  FileStream << std::endl;

  FileStream << "// Include" << std::endl;
  FileStream << "#include \"RichDet/Rich1DTabFunc.h\"" << std::endl;
  FileStream << std::endl;

  FileStream << "// Create object" << std::endl;
  FileStream << "/**" << std::endl;
  FileStream << " * @brief flattening function of the GhostProbability" << std::endl;
  FileStream << " *" << std::endl;
  FileStream << " * @return instance of function. Gives ownership! User has to delete!" << std::endl;
  FileStream << " */" << std::endl;
  FileStream << "Rich::TabulatedFunction1D* " << _FunctionName << "() {" << std::endl;
  FileStream << std::endl;

  FileStream << "// Initialisation (should do this just once, slow)" << std::endl;
  FileStream << "  double input[" << _nobin + 1 << "] = {";
  for (int i = 0; i < _nobin; i++) {
    FileStream << input[i] << ", ";
  }
  FileStream << input[_nobin] << "};" << std::endl;
  FileStream << "double output[" << _nobin + 1 << "] = {";
  for (int i = 0; i < _nobin; i++) {
    FileStream << output[i] << ", ";
  }
  FileStream << output[_nobin] << "};" << std::endl;
  FileStream << "return new Rich::TabulatedFunction1D(input, output, " << _nobin + 1 << ", gsl_interp_linear);" << std::endl;
  FileStream << "}" << std::endl;
  FileStream << std::endl;
  FileStream.close();
}

void GhostProb_flat::Terminate() {
  // The Terminate() function is the last function to be called during
  // a query. It always runs on the client, it can be used to present
  // the results graphically or save the results to file.
  _ghostprob_hists->saveHistograms(_outputfile.c_str());
}
