#define GhostProb_comp_cxx
// The class definition in GhostProb_comp.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.

// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// Root > T->Process("GhostProb_comp.C")
// Root > T->Process("GhostProb_comp.C","some options")
// Root > T->Process("GhostProb_comp.C+")
//

#include "GhostProb_comp.h"
#include <TH2.h>
#include <TStyle.h>

void GhostProb_comp::Begin(TTree * /*tree*/) {
  // The Begin() function is called at the start of the query.
  // When running with PROOF Begin() is only called on the client.
  // The tree argument is deprecated (on PROOF 0 is passed).
  TString option = GetOption();

  system("mkdir -p outputs/");

  ReadParameters();
  Init_hist();

  n_events = 0;
  n_cuts   = 0;
}

void GhostProb_comp::SlaveBegin(TTree * /*tree*/) {
  // The SlaveBegin() function is called after the Begin() function.
  // When running with PROOF SlaveBegin() is called on each slave server.
  // The tree argument is deprecated (on PROOF 0 is passed).
  TString option = GetOption();
}

Bool_t GhostProb_comp::Process(Long64_t entry) {
  // The Process() function is called for each entry in the tree (or possibly
  // keyed object in the case of PROOF) to be processed. The entry argument
  // specifies which entry in the currently loaded tree is to be processed.
  // It can be passed to either GhostProb_comp::GetEntry() or TBranch::GetEntry()
  // to read either all or the required parts of the data. When processing
  // keyed objects with PROOF, the object is already loaded and is available
  // via the fObject pointer.
  //
  // This function should contain the "body" of the analysis. It can contain
  // simple or elaborate selection criteria, run algorithms on the data
  // of the event and typically fill histograms.
  //
  // The processing can be stopped by calling Abort().
  //
  // Use fStatus to set the return value of TTree::Process().
  //
  // The return value is currently not used.

  fChain->GetTree()->GetEntry(entry);
  n_events++;
  if (n_events % 1000000 == 0)
    printf("Processing %2d.0 M events!\n", (n_events / 1000000));

  // save trks information to a vector
  vector<Float_t> trk_inf;
  trk_inf.clear();

  // double oneoverpt = 0.;
  // if(tracks_PT > 0)
  //   oneoverpt = 1./tracks_PT;

  if (tracks_TRACK_Type == 1) { // velo -- 11
    trk_inf.push_back(UpgradeGhostInfo_obsVP);
    trk_inf.push_back(UpgradeGhostInfo_FitVeloChi2);
    trk_inf.push_back(UpgradeGhostInfo_FitVeloNDoF);
    trk_inf.push_back(UpgradeGhostInfo_veloHits);
    trk_inf.push_back(UpgradeGhostInfo_utHits);
    trk_inf.push_back(TRACK_CHI2);
    trk_inf.push_back(TRACK_NDOF);
    trk_inf.push_back(TRACK_ETA);
  } else if (tracks_TRACK_Type == 3) { // long -- 21
    trk_inf.push_back(UpgradeGhostInfo_obsVP);
    trk_inf.push_back(UpgradeGhostInfo_FitVeloChi2);
    trk_inf.push_back(UpgradeGhostInfo_FitVeloNDoF);
    trk_inf.push_back(UpgradeGhostInfo_obsFT);
    trk_inf.push_back(UpgradeGhostInfo_FitTChi2);
    trk_inf.push_back(UpgradeGhostInfo_FitTNDoF);
    trk_inf.push_back(UpgradeGhostInfo_obsUT);
    trk_inf.push_back(UpgradeGhostInfo_FitMatchChi2);
    trk_inf.push_back(UpgradeGhostInfo_UToutlier);
    trk_inf.push_back(UpgradeGhostInfo_veloHits);
    trk_inf.push_back(UpgradeGhostInfo_utHits);
    trk_inf.push_back(TRACK_CHI2);
    trk_inf.push_back(TRACK_PT);
    trk_inf.push_back(TRACK_ETA);
  } else if (tracks_TRACK_Type == 4) { // upstream -- 13
    trk_inf.push_back(UpgradeGhostInfo_obsVP);
    trk_inf.push_back(UpgradeGhostInfo_FitVeloChi2);
    trk_inf.push_back(UpgradeGhostInfo_FitVeloNDoF);
    trk_inf.push_back(UpgradeGhostInfo_obsUT);
    trk_inf.push_back(UpgradeGhostInfo_UToutlier);
    trk_inf.push_back(UpgradeGhostInfo_veloHits);
    trk_inf.push_back(UpgradeGhostInfo_utHits);
    trk_inf.push_back(TRACK_CHI2);
    trk_inf.push_back(TRACK_NDOF);
    trk_inf.push_back(TRACK_PT);
    trk_inf.push_back(TRACK_ETA);
  } else if (tracks_TRACK_Type == 5) { // downstream -- 15
    trk_inf.push_back(UpgradeGhostInfo_obsFT);
    trk_inf.push_back(UpgradeGhostInfo_FitTChi2);
    trk_inf.push_back(UpgradeGhostInfo_FitTNDoF);
    trk_inf.push_back(UpgradeGhostInfo_obsUT);
    trk_inf.push_back(UpgradeGhostInfo_UToutlier);
    trk_inf.push_back(UpgradeGhostInfo_veloHits);
    trk_inf.push_back(UpgradeGhostInfo_utHits);
    trk_inf.push_back(TRACK_CHI2);
    trk_inf.push_back(TRACK_PT);
    trk_inf.push_back(TRACK_ETA);
  } else if (tracks_TRACK_Type == 6) { // TTrack -- 14
    trk_inf.push_back(UpgradeGhostInfo_obsFT);
    trk_inf.push_back(UpgradeGhostInfo_FitTChi2);
    trk_inf.push_back(UpgradeGhostInfo_FitTNDoF);
    trk_inf.push_back(UpgradeGhostInfo_veloHits);
    trk_inf.push_back(UpgradeGhostInfo_utHits);
    trk_inf.push_back(TRACK_CHI2);
    trk_inf.push_back(TRACK_NDOF);
    trk_inf.push_back(TRACK_PT);
    trk_inf.push_back(TRACK_ETA);
  }

  // signal or background
  string evt_type = "sig";
  if (tracks_assoc < 0.5)
    evt_type = "bkg";

  // get input variable names
  vector<string> varnames;
  varnames.clear();
  int trk_index = const_num::TRKTypeToNum((int)(tracks_TRACK_Type));
  varnames      = const_num::Get_TMVA_Variables(trk_index);

  // get hist name to fill
  for (int i = 0; i < varnames.size(); i++) {
    char histname[500];
    sprintf(histname, "%s_TRK%d_%s", varnames[i].c_str(), (int)(tracks_TRACK_Type), evt_type.c_str());

    // fill histograms
    // cout<<histname<<"  "<<trk_inf[i]<<"  "<<tracks_TRACK_Type<<endl;
    _ghostprob_hists->getTH1DptrByName(histname)->Fill(trk_inf[i]);
  }

  return kTRUE;
}

void GhostProb_comp::SlaveTerminate() {
  // The SlaveTerminate() function is called after all entries or objects
  // have been processed. When running with PROOF SlaveTerminate() is called
  // on each slave server.

  cout << "Processed "
       << n_events << " track candidates" << endl;

  // add variable to the fitting
}

void GhostProb_comp::Terminate() {
  // The Terminate() function is the last function to be called during
  // a query. It always runs on the client, it can be used to present
  // the results graphically or save the results to file.
  // outputFile->Write();

  _ghostprob_hists->saveHistograms(_outputfile);
}
