//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Nov  2 15:59:17 2015 by ROOT version 5.34/32
// from TTree tracks/tracks
// found on file: ../myalg.root
//////////////////////////////////////////////////////////

#ifndef GhostProb_plot_h
#define GhostProb_plot_h

#include <iostream>

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <TSelector.h>
// #include <TGraphSymmErrors.h>
#include "TGraphErrors.h"
#include <TGraph.h>
#include <iostream>

#include "PParameterReader.hpp"
#include "const_numbers.hpp"
#include "histo_Handle.hpp"

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

using namespace std;
using namespace const_num;
class GhostProb_plot : public TSelector {
public:
  TTree *fChain; //! pointer to the analyzed TTree or TChain

  // Declaration of leaf types
  Float_t tracks_TRACK_CHI2NDOF;
  Int_t   tracks_assoc;
  Float_t ghostprob;
  Float_t tracks_PT;
  Int_t   tracks_TRACK_Type;
  Float_t new_ghostprob;

  // List of branches
  TBranch *b_tracks_TRACK_CHI2NDOF; //!
  TBranch *b_tracks_assoc;          //!
  TBranch *b_ghostprob;             //!
  TBranch *b_tracks_PT;             //!
  TBranch *b_tracks_TRACK_Type;     //!
  TBranch *b_new_ghostprob;         //!

  GhostProb_plot(TTree * /*tree*/ = 0) : fChain(0) {}
  virtual ~GhostProb_plot() {}
  virtual Int_t  Version() const { return 2; }
  virtual void   Begin(TTree *tree);
  virtual void   SlaveBegin(TTree *tree);
  virtual void   Init(TTree *tree);
  virtual Bool_t Notify();
  virtual Bool_t Process(Long64_t entry);
  virtual Int_t  GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
  virtual void   SetOption(const char *option) { fOption = option; }
  virtual void   SetObject(TObject *obj) { fObject = obj; }
  virtual void   SetInputList(TList *input) { fInput = input; }
  virtual TList *GetOutputList() const { return fOutput; }
  virtual void   SlaveTerminate();
  virtual void   Terminate();
  void           setParameterFileName(const char *s) { _parameter_filename = TString(s); }
  void           setTrackType(int ttype) { _trk_type = ttype; }

private:
  TString _parameter_filename;
  void    ReadParameters();

  TString _outputfile;
  TString _EvtType;
  void    Init_hist();

  histo_Handle *_ghostprob_hists;

  int  n_events;
  int  n_cuts;
  int  _trk_type;
  bool _MakeSBPlot;

  struct Prob_info {
    double probA;
    double probB;
    double probC;
  };
  Prob_info              _gp_infor;
  std::vector<Prob_info> _gp_sig;
  std::vector<Prob_info> _gp_bkg;

  TGraphErrors *GP_Make_Efficiency(int sig[_NBINS], int bkg[_NBINS], string nametype);
  // void GP_Make_Efficiency(int sig[_NBINS], int bkg[_NBINS], TGraphErrors* gr);

  ClassDef(GhostProb_plot, 0);
};

#endif

#ifdef GhostProb_plot_cxx
void GhostProb_plot::Init(TTree *tree) {
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set branch addresses and branch pointers
  if (!tree)
    return;
  fChain = tree;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("tracks_TRACK_CHI2NDOF", &tracks_TRACK_CHI2NDOF, &b_tracks_TRACK_CHI2NDOF);
  fChain->SetBranchAddress("tracks_assoc", &tracks_assoc, &b_tracks_assoc);
  fChain->SetBranchAddress("ghostprob", &ghostprob, &b_ghostprob);
  fChain->SetBranchAddress("tracks_PT", &tracks_PT, &b_tracks_PT);
  fChain->SetBranchAddress("tracks_TRACK_Type", &tracks_TRACK_Type, &b_tracks_TRACK_Type);
  fChain->SetBranchAddress("new_ghostprob", &new_ghostprob, &b_new_ghostprob);
}

Bool_t GhostProb_plot::Notify() {
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void GhostProb_plot::ReadParameters() {
  PParameterReader parm(_parameter_filename.Data());

  _EvtType         = parm.GetChar("EvtType", "Data");
  string _Evttype_ = _EvtType.Data();

  _outputfile = parm.GetChar("Output", "Data");
  char nameout[200];
  sprintf(nameout, "%s%d.root", _outputfile.Data(), _trk_type);
  _outputfile      = (TString)nameout;
  _ghostprob_hists = new histo_Handle();

  _MakeSBPlot = parm.GetBool("MakeSBPlot", false);

  _gp_sig.clear();
  _gp_bkg.clear();
}

void GhostProb_plot::Init_hist() {
  _ghostprob_hists->AddHist1D("probA_sig", "", "prob", "", 100, 0, 1);
  _ghostprob_hists->AddHist1D("probB_sig", "", "prob", "", 100, 0, 1);
  _ghostprob_hists->AddHist1D("probC_sig", "", "prob", "", 100, 0, 20);
  _ghostprob_hists->AddHist1D("probA_bkg", "", "prob", "", 100, 0, 1);
  _ghostprob_hists->AddHist1D("probB_bkg", "", "prob", "", 100, 0, 1);
  _ghostprob_hists->AddHist1D("probC_bkg", "", "prob", "", 100, 0, 20);
  _ghostprob_hists->AddHist1D("EffA_sig", "", "prob", "", _NBINS, 0, 1);
  _ghostprob_hists->AddHist1D("EffA_bkg", "", "prob", "", _NBINS, 0, 1);
  _ghostprob_hists->AddHist1D("EffB_sig", "", "prob", "", _NBINS, 0, 1);
  _ghostprob_hists->AddHist1D("EffB_bkg", "", "prob", "", _NBINS, 0, 1);
  _ghostprob_hists->AddHist1D("EffC_sig", "", "prob", "", _NBINS, 0, 1);
  _ghostprob_hists->AddHist1D("EffC_bkg", "", "prob", "", _NBINS, 0, 1);

  if (_trk_type == 0) {
    char name[200];
    for (int i = 0; i < 5; i++) {
      int trk_type = const_num::NumToTRKType(i);
      sprintf(name, "probA_sig_Trk%d", trk_type);
      _ghostprob_hists->AddHist1D(name, "", "prob", "", 100, 0, 1);
      sprintf(name, "probA_bkg_Trk%d", trk_type);
      _ghostprob_hists->AddHist1D(name, "", "prob", "", 100, 0, 1);
      sprintf(name, "probB_sig_Trk%d", trk_type);
      _ghostprob_hists->AddHist1D(name, "", "prob", "", 100, 0, 1);
      sprintf(name, "probB_bkg_Trk%d", trk_type);
      _ghostprob_hists->AddHist1D(name, "", "prob", "", 100, 0, 1);

      // ghost rate as a function of pt and y
      sprintf(name, "PT_Trk%d_sig", trk_type);
      _ghostprob_hists->AddHist1D(name, "", "PT", "", 100, 0, 4000);
      sprintf(name, "PT_Trk%d_bkg", trk_type);
      _ghostprob_hists->AddHist1D(name, "", "PT", "", 100, 0, 4000);
      sprintf(name, "PT_Trk%d_sigPass", trk_type);
      _ghostprob_hists->AddHist1D(name, "", "PT", "", 100, 0, 4000);
      sprintf(name, "PT_Trk%d_bkgPass", trk_type);
      _ghostprob_hists->AddHist1D(name, "", "PT", "", 100, 0, 4000);
      sprintf(name, "PT_Trk%d_Ratio", trk_type);
      _ghostprob_hists->AddHist1D(name, "", "PT", "", 100, 0, 4000);
      sprintf(name, "PT_Trk%d_CutRatio", trk_type);
      _ghostprob_hists->AddHist1D(name, "", "PT", "", 100, 0, 4000);
    }
  }
}

#endif // #ifdef GhostProb_plot_cxx
