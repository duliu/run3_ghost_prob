## gcc
GCCVERSION=LCG_101/x86_64-centos7-gcc8-opt
source /cvmfs/sft.cern.ch/lcg/views/${GCCVERSION}/setup.sh

## root
export ROOTSYS=/cvmfs/sft.cern.ch/lcg/releases/LCG_102/ROOT/6.26.04/x86_64-centos8-gcc11-opt
export PATH=$PATH:$ROOTSYS/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib
