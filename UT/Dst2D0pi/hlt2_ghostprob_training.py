###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# configuration
from optparse import OptionParser
parser = OptionParser()
parser.add_option(
    "-d", "--data", dest="datafile", default="/afs/cern.ch/user/d/duliu/eos/Duanqing_work/service_work/new_data_sets/Dst2D0pi/ghostprob_test_training_tuple_expected_2024_minbias_xdigi.root")
parser.add_option("-m", "--model", dest="model", default="Long")
parser.add_option("-e", "--epochs", dest="epochs", type='int', default=100)
parser.add_option(
    "-s", "--save", action="store_true", dest="save", default=True)
parser.add_option(
    "-f", "--file", dest="weightsfile", default="ghostprob_test_weights_Long_Dst2D0pi.json")
parser.add_option(
    "--low_verbosity", action="store_false", dest="verbose", default=True)
parser.add_option("--deterministic", action="store_true", dest="det")
parser.add_option("--seed", dest="seed", type='int', default=0)
(options, args) = parser.parse_args()

model_name = options.model

# ensure for regression testing option of deterministic behaviour
import numpy as np
import torch, random

generator = torch.Generator()

if options.det:
    torch.use_deterministic_algorithms(True)
    random.seed(options.seed)
    np.random.seed(options.seed)
    torch.manual_seed(options.seed)
    generator.manual_seed(options.seed)


def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2**32
    np.random.seed(worker_seed)
    random.seed(worker_seed)


# import data
import ROOT as R
rdf = R.RDataFrame(f'TrainingTupleAlg_GhostProb_{model_name}/TrainingTuple',
                   options.datafile)
rdf_columns = rdf.GetColumnNames()


# import model
def get_model(name):
    if name == 'Long_noUT':
        from TrackTools.ghostprob_models import GhostProb_Long_noUT as model
        return model
    elif name == 'Long':
        from TrackTools.ghostprob_models import GhostProb_Long as model
        return model
    elif name == 'Downstream':
        from TrackTools.ghostprob_models import GhostProb_Downstream as model
        return model
    elif name == 'Upstream':
        from TrackTools.ghostprob_models import GhostProb_Upstream as model
        return model
    elif name == 'Ttrack':
        from TrackTools.ghostprob_models import GhostProb_Ttrack as model
        return model
    elif name == 'Velo':
        from TrackTools.ghostprob_models import GhostProb_Velo as model
        return model
    return None


model = get_model(model_name)
feats = model.features()

print(f"Defined model '{model_name}' with features:", feats)
print("model coverted to PyTorch:", model)

# training data
import pandas as pd
others = ['MC_TRUEID', 'MC_MOTHER_ID', 'MC_GMOTHER_ID', 'MC_TRUE_P']
obs = feats + others

df = pd.DataFrame(rdf.AsNumpy(obs))

base_sel = (df['MC_TRUE_P'] > -1)

labels = ['sig', 'bkg']
df['sig'] = np.where(
    (np.abs(df['MC_TRUEID']) != 0) & (np.abs(df['MC_TRUEID']) != 11) & (np.abs(df['MC_MOTHER_ID']) == 421)  & (np.abs(df['MC_GMOTHER_ID']) == 413), True, False)
df['bkg'] = np.where((np.abs(df['MC_TRUEID']) == 0), True, False)
df['flag'] = np.where(df['sig'], True, False)
selected_df = df[(df['sig'] | df['bkg']) & base_sel]

datafeats = selected_df[feats]
dataflag = selected_df[['flag']]

nsig = selected_df['sig'].sum()
nbkg = selected_df['bkg'].sum()
print(f"Number of signal is {nsig}; number of background: {nbkg}")

# convert to PyTorch compatible data
import copy
from torch import nn
from torch.utils.data import DataLoader, TensorDataset

X = torch.tensor(datafeats.values, dtype=torch.float32)
y = torch.tensor(dataflag.values, dtype=torch.float32)

# train test split
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(
    X,
    y,
    train_size=0.7,
    shuffle=True,
    random_state=options.seed if options.det else None)


# training model
def train(dataloader,
          model,
          loss_func,
          epochs=10,
          learning_rate=1e-3,
          X_val=None,
          y_val=None,
          verbose=True):
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

    # hold the best model
    best_acc = -np.inf
    best_weights = None

    print(f"Starting training (with {epochs} epochs)")
    for epoch in range(epochs):
        model.train()
        for inputs, targets in dataloader:
            # forward pass
            prediction = model(inputs)
            loss = loss_func(prediction, targets)
            # backward pass
            optimizer.zero_grad()
            loss.backward()
            # update weights
            optimizer.step()
        # accuracy evaluation at each epoch
        model.eval()
        prediction = model(X_val)
        acc = (prediction.round() == y_val).float().mean()
        acc = float(acc)
        if verbose and (epoch < 10 or epoch % 10 == 0):
            print(f"epoch #{epoch:3}: accuracy is {acc:.5f}")
        if acc > best_acc:
            best_acc = acc
            best_weights = copy.deepcopy(model.state_dict())

    # restore model and return best accuracy
    model.load_state_dict(best_weights)


batch_size = 64
train_dataset = TensorDataset(X_train, y_train)
train_dataloader = DataLoader(
    train_dataset,
    batch_size=batch_size,
    worker_init_fn=seed_worker,
    generator=generator)

train(
    train_dataloader,
    model,
    loss_func=nn.BCELoss(),
    epochs=options.epochs,
    X_val=X_test,
    y_val=y_test,
    verbose=options.verbose)

# check performance
from sklearn.metrics import roc_curve, roc_auc_score

pred_test = model(X_test)
output = pred_test.cpu().detach().numpy().transpose()[0]
flags = y_test.numpy().transpose()[0]
pred_dataset = pd.DataFrame(data={'output': output, 'flag': flags})

fpr, tpr, thresholds = roc_curve(pred_dataset['flag'], pred_dataset['output'])
auc = roc_auc_score(pred_dataset['flag'], pred_dataset['output'])

print(f"Result of training: AUC of ROC = {auc:.3f}")

# add flattening layer
flatten_data = pred_dataset[pred_dataset['flag'] == 0]
model.add_flatten_layer(flatten_data['output'], [0, 1], smoothen=True)

# save weights
if options.save: model.write_to_json(options.weightsfile)
