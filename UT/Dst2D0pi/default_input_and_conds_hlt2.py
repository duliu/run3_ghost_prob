###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from RecoConf.decoders import default_VeloCluster_source

# Use velo retina decoding:
default_VeloCluster_source.global_bind(bank_type="VPRetinaCluster")

file_paths = []
base_path = "/afs/cern.ch/user/d/duliu/eos/Duanqing_work/xia_bookkeeping/Dst2D0pi/"
for i in range(0, 22):
    file_path = f"{base_path}Dst2D0pi_{i}.xdigi"
    file_paths.append(file_path)

options.input_files = file_paths
## options.input_files = ["/afs/cern.ch/user/d/duliu/eos/Duanqing_work/xia_bookkeeping/00153948_00000272_1.xdigi",]
options.evt_max = -1
options.simulation = True
options.dddb_tag = 'dddb-20240311'
options.conddb_tag = 'sim-20231017-vc-mu100'
options.input_type = 'ROOT'
