## 1. Generate ROOT instead of JSON

`./Moore/build.x86_64_v2-centos7-gcc12+detdesc-opt/run gaudirun.py hlt2_ghostprob_training_data.qmt`

## 2. Generate JSON

`./Moore/build.x86_64_v2-centos7-gcc12+detdesc-opt/run gaudirun.py hlt2_ghostprob_training.py`

## 3. Generate ROOT with JSON

`./Moore/build.x86_64_v2-centos7-gcc12+detdesc-opt/run gaudirun.py hlt2_ghostprob_training_data.qmt`

## 4. Draw ROC

`./Moore/build.x86_64_v2-centos7-gcc12+detdesc-opt/run gaudirun.py hlt2_ghostprob_training_draw_ROC.py`
